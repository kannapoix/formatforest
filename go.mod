module formatforest.com

go 1.14

require (
	github.com/gomarkdown/markdown v0.0.0-20200316172748-fd1f3374857d
	github.com/otiai10/copy v1.1.1
)
